# Guidelines status

* ✅ Finito 
* ⚠️ WIP
* ❗️ Da fare

# Code guide lines - Frontend

* Sviluppo UX/UI
    * ✅ Realizzazione mockups
    * ✅ Realizzazione icone
    * ✅ Distribuzione
* Sviluppo frontend
    * ✅ Prefazione
    * ⚠️ Retrocompatibilità garantita abitualmente
    * Html
        * ⚠️ Risorse e strumenti
        * ⚠️ Commenti
        * ✅ Indentazione
        * ✅ Apici
        * ⚠️ Sintassi
        * ✅ Immagini responsive
        * ⚠️ ARIA | ADA
    * Scss
        * ⚠️ Risorse e strumenti
        * ⚠️ Commenti
        * ⚠️ Indentazione
        * ⚠️ Apici
        * ⚠️ Sintassi
    * Javascript
        * ✅ Risorse e strumenti
        * ⚠️ Spazi e indentazione
        * ⚠️ Commenti
        * ⚠️ Apici
        * ✅ Sintassi
        * ✅ Controllo di tipo
        * ✅ Esecuzione condizionale
        * ✅ Stile pratico
        * ✅ Nomenclatura
    * Node.js
        * ❗️ Compilazione e lint di file html
        * ❗️ Compilazione e lint di file scss
        * ❗️ Compilazione e lint di file js
        * ❗️ Minificazione di immagini
        * ❗️ Compilazione di file svg
        * ⚠️ Processo di pubblicazione
        * ⚠️ Processo di testing

---
## Sviluppo UX/UI

#### Realizzazione mockups

È necessario che responsabile frontend spenda del tempo con l'art director di riferimento, prima dell'inizio dello sviluppo dei mockups, affinchè possa fornire tutte informazioni utili sulla piattaforma di destinazione e risolvere eventuali dubbi

È necessario adottare i principi di atomic design ed utilizzare una griglia a 12 colonne

È consigliabile suddividere i rilasci grafici in molti "microdrop" piuttosto che pochi "macrodrop". I "microdrop" sono più veloci da realizzare, analizzare ed eventualmente correggere	

È necessario realizzare un primo mockup di linea guida (UIKit) che contenga tutti i componenti base del progetto (pulsanti, titoli, margini, input di testo, checkbox, etc). Tale approccio facilita l’analisi iniziale del progetto, permettendo di tenere il codice pulito, performante e declinabile nelle varie sezioni. L'UIKit non sostituisce le grafiche del sito, bensì permette di ovviare alla fase di sviluppo statico, permettendoci di lavorare all'interno della piattaforma sulla base di solide fondamenta.

I breakpoint da utilizzare variano in base alle necessità, quelli maggiormente utilizzati sono: 
* `320` - viewport _smartphone_ minima
* `768` - viewport _tablet_ più comune 
* `1280` o `1440` - viewport _desktop_ più comune

I mockups vanno realizzati in versione @1x, eventuali assets @2x consegnati a parte
	
#### Realizzazione icone
* Non devono contenere né tracciati aperti né linee 
* Tutti gli elementi che compongono l’icona devono essere riempimenti
* I vettoriali creati prima di essere esportati, devono essere allineati alla griglia pixel per non mostrare l’effetto sfocato ai bordi 
* Le misure solitamente rispettano i multipli di 8px
* Le linee realizzate non dovrebbero essere sotto i 2px
* I file devono essere monocolore (nero), verranno poi stilati a codice in base alle esigenze dei PSD 
* Le icone devono essere grandi quanto il file stesso

#### Distribuzione
* L’utilizzo di servizi come “Invision e/o Zeplin” sono i benvenuti, in alternativa, se viene usato una cartella salvata in cloud o un repo SVN è necessario strutturare i folder seguendo questi principi:
    * Suddividere in folder in base ai breakpoint utilizzati: 
		* `desktop_1920`
		* `tablet_768`
		* `smartphone_320`
	* Utilizzare nomi univoci per i design realizzati
	* È consigliato utilizzare lo stesso format per nominare psd e.g. -> “pagename_breakpoint_version.psd”, “pagename” dovrà essere mantenuto in tutti i folder: 
		* `desktop_1920/homepage_1920_v1.psd`
		* `tablet_768/homepage_768_v1.psd`
		* `smartphone_320/homepage_320_v1.psd `

---
## Sviluppo frontend

#### Prefazione

> **Tutto il codice**, in qualsiasi linguaggio sia, **dovrebbe sembrare come scritto da un'unica persona**, non importa quante persone vi abbiano contribuito._

> Le seguenti sezioni evidenziano una _ragionevole_ guida di stile per uno sviluppo moderno e coerente di applicazioni web, e non sono intese per essere obbligatorie. **Il concetto più importante è la consistenza dello stile nel codice**. Qualsiasi stile scegliate per i vostri progetti dovrebbe essere considerato legge.

### Retrocompatibilità garantita abitualmente

* Internet explorer >= `?`
* Edge >= `?`
* Chrome >= `?`
* Firefox >= `?`
* Safari >= `?`
* IOS Safari >= `?`
* Chrome Android >= `?`

`@TODO Quali settiamo?`

### Html

#### Risorse e strumenti

Sfrutta sempre un grid system a 12 colonne.

Apprezzato l'utilizzo di framework:
* [Foundation](https://foundation.zurb.com)
* [Bootstrap](https://getbootstrap.com)
* [Materialize](http://materializecss.com)

`@TODO Vogliamo tenerne solo uno?`
`@TODO Vogliamo aggiungerne altri?`

Usa [modernizr](https://modernizr.com) per gestire eventuali sistemi di fallback. 
Usa *Build custom* strettamente relazionate alle esigenze del progetto di appartenenza

`@TODO Quali altri?`
`@TODO Condividiamo build di basilare di esempio?`

#### Commenti

I commenti corti dovrebbero essere scritti in una singola riga
```html
<!-- This is a comment -->
```

I commenti più lunghi dovrebbero essere scritti in più righe nel seguente modo

```html
<!-- 
  Questo è un commento lungo. Questo è un commento lungo. Questo è un commento lungo.
  Questo è un commento lungo. Questo è un commento lungo. Questo è un commento lungo.
-->
```

#### Indentazione

Indenta coerentemente il codice per aumentarne la leggibilità

```html
<!-- Sbagliato -->
<ul>
    <li>General</li><li>The root Element</li><li>Sections</li> ...
</ul>

<!-- Corretto -->
<ul>
    <li>General</li>
    <li>The root Element</li>
    <li>Sections</li>
    ...
</ul>
```

#### Apici

Per i file `.html` viene considerato standard l'utilizzo del doppio apice

```html
<!-- Sbagliato -->
<div class='foo'> ... </div>

<!-- Corretto -->
<div class="foo"> ... </div>
```

#### Sintassi

Utilizzare caratteri minuscoli per `tag` e `attributi`

```html
<!-- Sbagliato -->
<SECTION> 
  <p> ... </p>
</SECTION>

<!-- Corretto -->
<section> 
  <p> ... </p>
</section>

<!-- Sbagliato -->
<div CLASS="menu"> ...

<!-- Corretto -->
<div class="menu"> ...
```

Chiudi tutti i `<tag>`

```html
<!-- Sbagliato -->
<meta charset="utf-8">

<!-- Corretto -->
<meta charset="utf-8" />
```

Posiziona il tag `</br>` simulandone la resa a frontend

```html
<!-- Sbagliato -->
<p>HTML</br>Best</br>Practices</p>

<!-- Corretto -->
<p>HTML</br>
Best</br>
Practices</p>
```

Non aggiungere spazi non necessari

```html
<!-- Sbagliato -->
<h1 class=" title " >HTML Best Practices</h1>

<!-- Corretto -->
<h1 class="title">HTML Best Practices</h1>


<!-- Sbagliato -->
<input   name="q"  type="search">

<!-- Corretto -->
<input name="q" type="search">
```

Ricordati di aggiungere l'attributo `alt` al tag `<img>`

```html
<!-- Sbagliato -->
<img src="/img/logo.png">

<!-- Corretto -->
<img alt="HTML Best Practices" src="/img/logo.png">
```

Non mischiare richieste di file `.js` con quelle `.css`

```html
<!-- Sbagliato -->
<script src="/js/jquery.min.js"></script>
<link href="/css/screen.css" rel="stylesheet">
<script src="/js/main.js"></script>

<!-- Corretto -->
<link href="/css/screen.css" rel="stylesheet">
<script src="/js/jquery.min.js"></script>
<script src="/js/main.js"></script>
```

Ricordati di includere il metatag `viewport` in tutte le tue pagine

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

#### Immagini responsive

Utilizza il tag `<picture>` per renderizzare immagini di differenti formati in base alla viewport, polyfill [picturefill](https://scottjehl.github.io/picturefill/) per garantire la retrocompatibilità.

```html
<picture>
    <source srcset="../img/sample.svg" media="(min-width: 768px)" />
    <img srcset="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
        alt="Sample pic" />
</picture>
```

#### ARIA

Alcuni tag hanno l'ARIA role implicito, non è necessario renderlo esplicito

```html
<!-- Sbagliato -->
<nav role="navigation">
  ...
</nav>

<hr role="separator">

<!-- Corretto -->
<nav>
  ...
</nav>

<hr>
```

Tutti i dettagli qui: https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques

`@TODO lista paesi obbligatoria per legge`

## Scss

#### Risorse, strumenti e requisiti

Sfruttare [csscomb](http://csscomb.com) per migliorare la consistenza dello stile nel codice

Non scrivere mai direttamente file `.css`, compilare sempre file `.scss`

Utilizzare un approccio responsive, **mobile first**

Utilizzare, dove possibile, animazioni che sfruttino la GPU e non la CPU

Strutturare le cartelle contenti i file `.scss` secondo principi di atomic design:

```
.
├── configs/
|   └── _configs.scss
|   └── _settings.scss
|   ├── _variables.scss
├── organisms/
|   ├── _breadcrumbs.scss
|   └── _header.scss
|   └── _organisms.scss
├── pages/
|   ├── _pages.scss
|   └── _homepage.scss
|   └── _pdp.scss
└── style.scss
```

Ogni cartella contiene un file avente lo stesso nome, che importa tutti gli altri:

```scss
// pages/_pages.scss
@import 'homepage';
@import 'pdp';

// style.scss
@import 'configs/configs';
@import 'organisms/organisms';
@import 'pages/pages';
```

### Manifesto idiomatico

`@TODO Manifesto scss`

## Javascript

#### Risorse e strumenti

* [jsPerf](http://jsperf.com/)
* [jsFiddle](http://jsfiddle.net/)
* [jsbin](http://jsbin.com/)
* [jshint](http://jshint.com/)
* [jslint](http://jslint.org/)
* [you might not need jquery](http://youmightnotneedjquery.com)
* [editorconfig](http://editorconfig.org/)

#### Spazi e indentazione

Mai mischiare spazi e tabulazioni.

Quando iniziate un progetto, prima di cominciare a scrivere il codice, scegliete tra indentazione soft (spazi) o tabulazioni reali, e considerate questa scelta **legge**.
* Per la leggibilità, raccomandiamo di impostare la dimensione dei rientri del proprio editor a `4` caratteri &mdash; questo significa

Se il vostro editor lo supporta, lavorate sempre con l'impostazione "mostra caratteri invisibili" attiva. I benefici di questa pratica sono:
* Rafforzata consistenza
* Eliminazione di spazi vuoti alla fine delle linee
* Eliminazione di linee vuote
* Commit e diff più facili da leggere

Usate [editorconfig](http://editorconfig.org/) quando possibile. Supporta la maggior parte degli IDE e gestisce la maggior parte delle impostazioni per gli spazi vuoti.

#### Commenti

Linea singola sopra il codice a cui ci si riferisce

Multilinea va bene

Commenti a fine linea sono proibiti!

Lo stile JSDoc va bene, ma richiede un significante investimento di tempo

#### Apici

Che preferiate singoli o doppi apici non importa, non c'è differenza di gestione da parte di JavaScript. Ciò che **deve assolutamente** essere mantenuta è la consistenza. **Mai mischiare gli apici nello stesso progetto. Scegliete uno stile e proseguite con esso.**

Fine delle linee e linee vuote

Lo spazio vuoto può complicare le diff e rendere le modifiche impossibili da leggere. Considerate di implementare un hook di pre-commit che rimuova automaticamente lo spazio vuoto alla fine delle linee e gli spazi vuoti nelle linee vuote.

#### Sintassi

Parentesi, parentesi graffe, interruzioni di linea

```javascript
// if/else/for/while/try hanno sempre spazi, parentesi graffe e sono suddivisi su
// più linee, questo incoragga la leggibilità

// Esempi di sintassi veramente illeggibile
if(condition) doSomething();
while(condition) iterating++;
for(var i=0;i<100;i++) someIterativeFn();

// Uso degli spazi vuoti per incrementare la leggibilità
if ( condizione ) {
    // istruzioni
}

while ( condizione ) {
    // istruzioni
}

for ( var i = 0; i < 100; i++ ) {
    // istruzioni
}

var prop;

for ( prop in object ) {
    // istruzioni
}

if ( true ) {
    // istruzioni
} else {
    // istruzioni
}
```

Assegnazioni, dichiarazioni, funzioni ( non anonima, espressione, costruttore )

```javascript

// Variabili
var foo = "bar",
    num = 1,
    undef;

// Notazioni literal:
var array = [],
    object = {};


// L'utilizzo di una sola `var` per scope (funzione) incrementa la leggibilità 
// e mantiene la tua lista di dichiarazioni ordinata (inoltre riduce il numero di digitazioni)

// Sbagliato
var foo = "";
var bar = "";
var qux;

// Corretto
var foo = "",
    bar = "",
    quux;


// Le dichiarazioni var dovrebbero sempre risiedere all'inizio del rispettivo scope (funzione).
// Lo stesso vale per const e let di ECMAScript 6.

// Sbagliato
function foo() {

    // alcune istruzioni qui

    var bar = "",
    qux;
}

// Corretto
function foo() {
    var bar = "",
    qux;

    // tutto il codice dopo le dichiarazioni delle variabili.
}

// const e let, da ECMAScript 6, 
// dovrebbero essere posizionate all'inizio del loro scope (blocco).

// Sbagliato
function foo() {
    let foo,
    bar;
    if ( condition ) {
        bar = "";
        // istruzioni
    }
}

// Corretto
function foo() {
    let foo;
    if ( condition ) {
        let bar = "";
        // istruzioni
    }
}
```

```javascript
// Dichiarazione di funzione non anonima
function foo( arg1, argN ) {

}

// Utilizzo
foo( arg1, argN );

// Dichiarazione di funzione non anonima
function square( number ) {
    return number * number;
}

// Utilizzo
square( 10 );

// Stile forzato di passaggio dei parametri a cascata
function square( number, callback ) {
    callback( number * number );
}

square( 10, function( square ) {
    // istruzioni di callback
});

// Funzione espressione
var square = function( number ) {
    // Ritorna qualcosa di importante e rilevante
    return number * number;
};

// Funzione espressione con identificatore, ha il valore aggiunto di essere in 
// grado di richiamare sé stessa ed ha un'identità nelle tracce dello stack
var factorial = function factorial( number ) {
    if ( number < 2 ) {
        return 1;
    }
    return number * factorial( number - 1 );
};

// Dichiarazione di costruttore
function FooBar( options ) {
    this.options = options;
}

// Utilizzo
var fooBar = new FooBar({ a: "alpha" });

fooBar.options;
// { a: "alpha" }

```

Eccezioni, lievi deviazioni

```javascript
// Funzioni con callback
foo(function() {
    // Nota che non ci sono spazi tra la prima parentesi
    // della funzione chiamata in esecuzione e la parola "function"
});

// Funzione che accetta un array, nessuno spazio
foo([ "alpha", "beta" ]);

// Funzione che accetta un oggetto, nessuno spazio
foo({
    a: "alpha",
    b: "beta"
});

// Singolo argomento stringa, nessuno spazio
foo("bar");

// Parentesi interne di raggruppamento, nessuno spazio
if ( !("foo" in obj) ) {

}

```

#### Controllo di tipo

Tipi attuali

```javascript
// Stringa:
typeof variable === "string"

// Numero:
typeof variable === "number"

// Booleano:
typeof variable === "boolean"

// Oggetto:
typeof variable === "object"

// Array:
Array.isArray( arrayLikeObject ) //(dove possibile)

// Nodo:
elem.nodeType === 1

// Nullo:
variable === null

// Nullo o non definito (undefined):
variable == null

// Non definito:

// Variabili globali:
typeof variable === "undefined"

// Variabili locali:
variable === undefined

// Proprietà:
object.prop === undefined
object.hasOwnProperty( prop )
"prop" in object
```

Tipi forzati

Considerate le implicazioni di ciò che segue... Dato questo codice `HTML`:

```html
<input type="text" id="foo-input" value="1">
```

```javascript

// `foo` è stato dichiarato di valore `0` ed il suo tipo è `number`
var foo = 0;

// typeof foo;
// "number"
...

// Successivamente, in qualche altra parte del codice, necessitate di aggiornare `foo`
// con un nuovo valore derivato da un elemento input

foo = document.getElementById("foo-input").value;

// Se testaste `typeof foo` adesso, il risultato sarebbe `string`
// Ciò significa che se aveste del codice che testa `foo` in questo modo:

if ( foo === 1 ) {
    importantTask();
}

// `importantTask()` non verrebbe mai eseguito, anche se `foo` fosse di valore "1"

// Potete prevenire questi problemi usando la forzatura intelligente con gli operatori unari + o -:

foo = +document.getElementById("foo-input").value;
//    ^ l'operatore + unario convertirà il suo operando destro in un numero

// typeof foo;
// "number"

if ( foo === 1 ) {
    importantTask();
}

// `importantTask()` verrà eseguito
```

Ecco alcuni casi comuni con forzature:


```javascript
var number = 1,
    string = "1",
    bool = false;

number;
// 1

number + "";
// "1"

string;
// "1"

+string;
// 1

+string++;
// 1

string;
// 2

bool;
// false

+bool;
// 0

bool + "";
// "false"
```


```javascript
var number = 1,
    string = "1",
    bool = true;

string === number;
// false

string === number + "";
// true

+string === number;
// true

bool === number;
// false

+bool === number;
// true

bool === string;
// false

bool === !!string;
// true
```

```javascript
var array = [ "a", "b", "c" ];

!!~array.indexOf("a");
// true

!!~array.indexOf("b");
// true

!!~array.indexOf("c");
// true

!!~array.indexOf("d");
// false
```

Tenete presente che i summenzionati esempi dovrebbero essere considerati come "eccessivamente chiari". È da preferire il più ovvio approccio di comparazione del valore ritornato da `indexOf`, cioè:

```javascript
if ( array.indexOf( "a" ) >= 0 ) {
    // ...
}
```

```javascript
var num = 2.5;

parseInt( num, 10 );

// è l'equivalente di...

~~num;

num >> 0;

num >>> 0;

// Tutti danno come risultato 2


// Comunque ricordate che i numeri negativi verranno trattati differentemente...

var neg = -2.5;

parseInt( neg, 10 );

// è l'equivalente di...

~~neg;

neg >> 0;

// Tutti danno come risultato -2
// Ma...

neg >>> 0

// Darà come risultato 4294967294

```

#### Esecuzione condizionale

```javascript
// Quando si deve solo stabilire se un array ha una dimensione,
// invece di questo:
if ( array.length > 0 ) ...

// ...verificate la verità condizionale, in questo modo:
if ( array.length ) ...


// Quando si deve solo stabilire che un array è vuoto,
// invece di questo:
if ( array.length === 0 ) ...

// ...verificate la verità condizionale, in questo modo:
if ( !array.length ) ...


// Quando si deve solo stabilire che una stringa non è vuota,
// invece di questo:
if ( string !== "" ) ...

// ...verificate la verità condizionale, in questo modo:
if ( string ) ...


// Quando si deve solo stabilire che una stringa _è_ vuota,
// invece di questo:
if ( string === "" ) ...

// ...verificate la verità condizionale, in questo modo:
if ( !string ) ...


// Quando si deve solo stabilire che un riferimento è vero,
// invece di questo:
if ( foo === true ) ...

// ...verificatelo per il suo significato, avvantaggiatevi delle funzionalità proprie del linguaggio:
if ( foo ) ...


// Quando si deve solo stabilire che un riferimento è falso,
// invece di questo:
if ( foo === false ) ...

// ...usate la negazione per forzare una valutazione vera
if ( !foo ) ...

// ...ma state attenti, questo varrà anche per: 0, "", null, undefined, NaN
// Se DOVETE testare per un falso booleano, allora usate
if ( foo === false ) ...


// Quando si deve solo stabilire che un riferimento potrebbe essere null o undefined,
// ma NON falso, "" o 0, invece di questo:
if ( foo === null || foo === undefined ) ...

// ...avvantaggiatevi della forzatura di tipo fatta da ==, in questo modo:
if ( foo == null ) ...

// Ricordate, l'uso di == farà corrispondere un `null` SIA a `null` che a `undefined`
// ma non a `false`, "" o 0
null == undefined
```
Valutate _sempre_ per il risultato migliore e più accurato - quella sopra è una linea guida, non un dogma.

Note sulla forzatura di tipo e sulla valutazione:

```javascript
// Preferite `===` a `==` (a meno che il caso non richieda una valutazione di tipo meno restrittiva)

// === non forza il tipo, ciò significa che:

"1" === 1:
// false

// == forza il tipo, ciò significa che:

"1" == 1;
// true


// Booleani, verità e falsità

// Booleani:
true, false

// Verità:
"foo", 1

// Falsità:
"", 0, null, undefined, NaN, void 0

```

#### Stile pratico

```javascript
// Un modulo pratico
(function( global ) {

    var Module = (function() {

        var data = "secret";

        return {
            // Proprietà booleana
            bool: true,
            // Un valore stringa
            string: "a string",
            // Una proprietà array
            array: [ 1, 2, 3, 4 ],
            // Una proprietà oggetto
            object: {
                lang: "en-US"
            },
            getData: function() {
                // ottiene il valore corrente di `data`
                return data;
            },
            setData: function( value ) {
                // imposta il valore di `data` e lo ritorna
                return ( data = value );
            }
        };

    })();

    // Altre istruzioni

    // esponiamo il nostro modulo all'oggetto globale
    global.Module = Module;

})( this );

```

```javascript

// Un costruttore pratico
(function( global ) {

    function Ctor( foo ) {
        this.foo = foo;
        return this;
    }

    Ctor.prototype.getFoo = function() {
        return this.foo;
    };

    Ctor.prototype.setFoo = function( val ) {
        return ( this.foo = val );
    };

    // Per richiamare il costruttore senza `new`, potreste fare così:
    var ctor = function( foo ) {
        return new Ctor( foo );
    };

    // esponiamo il nostro costruttore all'oggetto globale
    global.ctor = ctor;

})( this );
```

#### Nomenclatura

Non siete un compilatore/compressore umano di codice, quindi non cercate di esserlo.
Il seguente codice è un esempio di tremenda nomenclatura:

```javascript
// Esempio di codice con nomi per nulla chiari
function q(s) {
    return document.querySelectorAll(s);
}
var i,a=[],els=q("#foo");
for(i=0;i<els.length;i++){a.push(els[i]);}
```

Sicuramente avrete visto codice come questo.
Ecco lo stesso pezzo di codice, ma con una nomencaltura più riflessiva ed esplicativa (ed una struttura leggibile):

```javascript
// Esempio di codice con nomi migliorati
function query( selector ) {
    return document.querySelectorAll( selector );
}

var idx = 0,
    elements = [],
    matches = query("#foo"),
    length = matches.length;

for( ; idx < length; idx++ ) {
    elements.push( matches[ idx ] );
}
```

Alcune ulteriori considerazioni:

```javascript
// Nominare stringhe
`dog` è una stringa

// Nominare array
`dogs` è un array di stringhe `dog`

// Nominare funzioni, oggetti, istanze, ecc.
camelCase; dichiarazioni di funzione e variabile

// Nominare costruttori, prototipi, ecc.
PascalCase; funzione costruttore

// Nominare espressioni regolari
rDesc = //;

// Dalla guida allo stile della libreria di Google Closure
functionNamesLikeThis;
variableNamesLikeThis;
ConstructorNamesLikeThis;
EnumNamesLikeThis;
methodNamesLikeThis;
SYMBOLIC_CONSTANTS_LIKE_THIS;
```

### Node.js

* Utilizzare [Node.js](https://nodejs.org/it/) per *elaborare, compilare, concatenare, ecc.* i file secondo i requisiti del progetto di appartenenza
* Tool maggiormente utilizzati:
    * [Gulp](https://gulpjs.com)
    * [Webpack](https://webpack.js.org)
    * [Babel](https://babeljs.io)

#### Processo di pubblicazione

Ogni progetto dovrebbe sempre cercare di fornire un qualche strumento generico, per mezzo del quale i sorgenti possano venire validati, testati e compressi per il successivo utilizzo in ambiente di produzione.

#### Processo di testing

`@TODO: Approfondire`

I progetti _devono_ includere una qualche sorta di unità, guida o implementazione di test oppure un test funzionale. Dimostrazioni di casi d'uso NON SONO QUALIFICATI come "test". La seguente è una lista di framework per il testing, nessuno dei quali è preferito rispetto all'altro.

 * [QUnit](http://github.com/jquery/qunit)
 * [Jasmine](https://github.com/pivotal/jasmine)
 * [Vows](https://github.com/cloudhead/vows)
 * [Mocha](https://github.com/visionmedia/mocha)
 * [Hiro](http://hirojs.com/)
 * [JsTestDriver](https://code.google.com/p/js-test-driver/)
 * [Buster.js](http://busterjs.org/)
 * [Sinon.js](http://sinonjs.org/)
